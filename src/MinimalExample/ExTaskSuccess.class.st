Class {
	#name : #ExTaskSuccess,
	#superclass : #Object,
	#category : #MinimalExample
}

{ #category : #'as yet unclassified' }
ExTaskSuccess >> onSuccess: successBlock onError: errorBlock [
	^ successBlock value
]
