Class {
	#name : #ExTask,
	#superclass : #Object,
	#instVars : [
		'block'
	],
	#category : #MinimalExample
}

{ #category : #'instance creation' }
ExTask class >> withBlock: block [
	^ self new block: block; yourself.
]

{ #category : #accessing }
ExTask >> block: aBlock [
	block := aBlock
]

{ #category : #accessing }
ExTask >> do [
	^block value
]
