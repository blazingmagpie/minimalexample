Class {
	#name : #ExTaskError,
	#superclass : #Object,
	#instVars : [
		'error'
	],
	#category : #MinimalExample
}

{ #category : #'instance creation' }
ExTaskError class >> withError: error [
	^ self new error: error; yourself.
]

{ #category : #accessing }
ExTaskError >> error: err [
	error := err
]

{ #category : #'as yet unclassified' }
ExTaskError >> onSuccess: successBlock onError: errorBlock [
	^ errorBlock cull: error
]
