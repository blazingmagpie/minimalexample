Class {
	#name : #ExScheduler,
	#superclass : #Object,
	#instVars : [
		'taskList'
	],
	#category : #MinimalExample
}

{ #category : #examples }
ExScheduler class >> example [
	<example>
	|scheduler|

	scheduler := self new.

	scheduler addTask: (ExTask withBlock: [ Transcript show: 'a']).
	scheduler addTask: (ExTask withBlock: [ Transcript show: 'b']).
	scheduler addTask: (ExTask withBlock: [ Transcript show: 'c']).
	scheduler addTask: (ExTask withBlock: [ 1/0 ]).
	
	scheduler doAll 
		onSuccess: [ Transcript cr; show: 'Success' ] 
		onError: [ :e | ^ e signalContext nextHandlerContext handleSignal: e ]
]

{ #category : #adding }
ExScheduler >> addTask: task [
	taskList add: task
]

{ #category : #adding }
ExScheduler >> doAll [
	[ 
		taskList do: [ :task | task do ].
		^ ExTaskSuccess new.
	] onErrorDo: [ :e | 
		^ ExTaskError withError: e
	] 
]

{ #category : #adding }
ExScheduler >> initialize [ 
	taskList := OrderedCollection new.
]
